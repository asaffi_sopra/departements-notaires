﻿<?php
/**
 * Formulaire de consultation des statistiques
 */
require_once 'notaires_fonctions.php';
require_once 'lib/GraphHelper.php';

sessionCheck();

if( isAdmin() || isGestionnaire() )
{
    $dateTo = new DateTime();
    $dateFrom = new DateTime();
    $dateFrom->sub(new DateInterval("P4M")); // affiche au maximum 5 mois
    $dateFrom->setDate($dateFrom->format("Y"), $dateFrom->format("m"), 1); // force au premier jour du mois
    $dateFrom->setTime(0, 0); // force l'heure à 00:00
    
    $statsRecherche = getStatsRecherche($dateFrom, $dateTo);
    $statsConnexion = getStatsConnexion($dateFrom, $dateTo);
    
    // Années disponibles pour export PDF
    $years = getAnneesStats();
    
    // Affichage du mois le plus récent d'abord
    $series = GraphHelper::getStatsSeries($dateTo, $dateFrom, $statsRecherche, $statsConnexion);

    echo templateRender("sys/statistiques.tpl", array("years" => $years, "series"=>$series));
}
else
{
    header ('Location: index.php');
}





?>