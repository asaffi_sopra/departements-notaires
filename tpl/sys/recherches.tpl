﻿
<style type="text/css" media="all">
/*Largeur de la liste déroulante*/

#nom_chosen.chosen-container-single .chosen-single span {
    width:150px;
}
#nom_chosen.chosen-container .chosen-drop {
    width:186px;
}

.chosen-container-single .chosen-single span {
    width:700px;
}
.chosen-container .chosen-drop {
    width:736px;
}
</style>

<div id=recherche_listing>
<form name="selecto" method=post action="index.php?index=recherches" onsubmit="return compare_Date('day_start','month_start','year_start','day_end','month_end','year_end');">
&nbsp;<u>Critère de recherche :</u><br><br>
{% if  profil_notaire==3 or profil_notaire==4 %}
&nbsp;<label for=type id=plop>Étude :</label>
<select name="selecto" class="chosen-select" />
    <option value=""  >Sélectionner une étude</option>
    {% for key,nom_indu in liste_deroulante_etude %}
        <option value="{{key}}" {% if selecto ==key %} selected="selected" {% endif %} >{{nom_indu}}</option>
    {% endfor %} 
</select>
<br/><br/>
{% endif %} 
&nbsp;<label for=type id=plop>Type de réponse :</label>
<select name="type_reponse" id="type_reponse" />
<option value="">Sélectionner un type de réponse</option>
<optgroup label="Type de réponse" >
<option value="4" {% if type_reponse =='4' %} selected="selected" {% endif %} >Ambigu</option>
 <option value="1" {% if type_reponse =='1' %} selected="selected" {% endif %} >Récupération</option>
  <option value="2" {% if type_reponse =='2' %} selected="selected" {% endif %} >Indus probable</option>
   <option value="3" {% if type_reponse =='3' %} selected="selected" {% endif %} >Inconnu</option>
  </optgroup>
   </select>
<label for=nom id=plop>Nom de l'individu :</label>
<select id="nom" name="nom" class="chosen-select" />
    <option value="" {% if nom =='' %} selected="selected" {% endif %} >Sélectionner un nom</option>
    {% for key,nom_indu in liste_deroulante_individu %}
        <option value="{{nom_indu}}" {% if nom ==nom_indu %} selected="selected" {% endif %} >{{nom_indu}}</option>
    {% endfor %} 
</select>
<br/>
<div id=listing_date_deb>
&nbsp;<label  id=listing for=day_start>Date de début : </label>
  <select id="day_start" name="day_start" />
		{% for i in 1..31 %}
            <option value="{{i}}" {% if day_start ==i %} selected="selected" {% endif %} >{{i}}</option>  
        {% endfor %}    
  </select>
  <select id="month_start" name="month_start" />
		{% for i in 1..12 %}
            <option value="{{i}}" {% if (month_start ==i) or (date|date('m') ==i and (month_start == null)) %} selected="selected" {% endif %} >{{mois_fr[i]}}</option>  
        {% endfor %}          
  </select> 
  <select id="year_start" name="year_start" />
	{% for key,annee in annee_listing %}
        <option value="{{annee}}" {% if (year_start==annee) or (date|date('Y')==annee and year_start==null) %} selected="selected" {% endif %} >{{annee}}</option>
    {% endfor %}
  </select>
  <label id=listing for=day_end>Date de fin : </label>
  <select id="day_end" name="day_end" />    
  {% for i in 1..31 %}
            <option value="{{i}}" {% if (day_end ==i) or (date|date('d')==i and day_end ==null) %} selected="selected" {% endif %} >{{i}}</option>  
        {% endfor %}    
  </select>
  <select id="month_end" name="month_end" /> 
        {% for i in 1..12 %}
            <option value="{{i}}" {% if (month_end ==i) or (date|date('m') ==i and month_end ==null) %} selected="selected" {% endif %} >{{mois_fr[i]}}</option>  
        {% endfor %}    
  </select>
  <select id="year_end" name="year_end" />
	{% for key,annee in annee_listing %}
        <option value="{{annee}}" {% if (year_end==annee) or (date|date('Y')==annee and year_end==null) %} selected="selected" {% endif %} >{{annee}}</option>
    {% endfor %}
  </select>
  <input type=submit id=submit_listing name=submit_listing value='Recherche'></input>
  
</div><!-- listing_date_deb -->

</div><!-- recerche_listing -->
</form>

<div id=tableau>
{{message | raw}}
</div>

<script type="text/javascript">
//Condition liste déroulante
var config = {
      '.chosen-select'           : {}
    }
    for (var selector in config) {
      $(selector).chosen(config[selector]);
    }
</script>


