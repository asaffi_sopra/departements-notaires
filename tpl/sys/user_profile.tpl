<h2>Information sur votre profil</h2>

<div id="cadre_profil">
  <table>
    <tr>
      <td>
        Identifiant :
      </td>
      <td>
        {{session.loginNotaire}}
      </td>
    </tr>
    <tr>
      <td>
        Email :
      </td>
      <td>
        {{session.mailNotaire}}
      </td>
    </tr>
    <tr>
      <td>
        Profil utilisateur :
      </td>
      <td>
        {{session.profile_label}}
      </td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td>Mot de passe :</td>
      <td><a href="index.php?index=modifier_mdp">Modifier mot de passe</a></td>
    </tr>
  </table>
</div>

