{# Corps mail confirmation création de compte #}

<style>
.corps {
  font-family: 'verdana';
  font-size: 80%;
}
.signature {
  font-family:'verdana';
  font-size:75%;
  font-weight:bold;
}

li {
  font-family:'verdana';
  font-size:80%;
}

</style>

<p class="corps">
Maître, <br/> 
<br/>
Le {{config.nom_departement_long}} met à votre disposition l'application <b>{{config.nom_application}}</b>.<br/> 
<br/>
Cette application web vous permet de connaitre instantanément l'existence d'une créance du Département au titre de 
l'aide sociale, pour une personne dont vous êtes en charge de la succession.<br/> 
<br/>
{{config.mail_creation_compte_intro|raw}}
<br/>
Voici les informations nécessaires à votre étude, pour la connexion à l'application <b>{{config.nom_application}}</b> :<br/>
<br/>
</p>
<ul> 
    <li>L'adresse internet de connexion à l’application est {{config.adresse_url_appli}}</li> 
    <li>Votre identifiant est votre code CRPCEN (ex : 69298)</li>
</ul> 


<p class="corps">
<b>Procédure pour activer votre compte :</b><br/>
</p>
<ol>
    <li>Cliquez sur le lien suivant et renseignez votre code CRPCEN : {{config.adresse_url_appli}}/index.php?index=mail_reinit</li>
    <li>Vous recevez alors, sur votre messagerie, un email ayant pour objet [<i>{{config.nom_application}} - Réinitialisation de votre mot de passe</i>].
        Cet email vous permet de créer votre mot de passe d’accès à l’application.</li>
    <li>Une fois votre mot de passe créé, cliquez sur « Réinitialiser » puis sur « Revenir à l’accueil ».</li>
    <li>Sur la page d’accueil, renseignez votre identifiant (code CRPCEN) et votre mot de passe nouvellement créé.</li>
    <li>Vous accédez alors à l’application.</ol>
</ol> 

<p class="corps">
Un document d'aide à l'utilisation est présent sur la page d'accueil de l'application.<br/>  
L'application a été optimisée pour une ouverture avec le navigateur Mozilla Firefox.<br/> 
<br/>
<b>Pour toute anomalie, merci de contacter le bureau suivi des dispositifs du {{config.nom_departement_long}} ({{config.mail_gestion}}).</b><br/>
<br/> 
{{config.mail_creation_compte_nota_bene|raw }}
<br />
<br /> Cordialement,<br/>
<br />
<b>{{config.mail_signature|raw}}</b>
</p>

