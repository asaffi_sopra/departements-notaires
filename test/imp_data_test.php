<?php
include_once '../notaires_fonctions.php';
// Insertion des données par rapport aux fichiers "demande.csv" et "individus.csv"

$fic_demande = "demande.csv";
$fic_individus = "individus.csv";

if ($handle = opendir ( '.' )) {
	while ( false !== ($entry = readdir ( $handle )) ) {
		if ($entry != "." && $entry != "..") {
			
			// Inssertion des données tests du fichier "demande.csv"
		  if ($entry == $fic_demande) {
			  $connect->query ("truncate table demande");
				$sql = "LOAD DATA LOCAL INFILE '$fic_demande'
						INTO TABLE demande FIELDS TERMINATED BY ';' ENCLOSED BY '\"'  LINES TERMINATED BY '\r\n' IGNORE 1 LINES
						(num_ind, sexe, nom_usage, nom_usage_sans, nom_civil, prenom, prenom_sans, annee_naissance, mois_naissance, jour_naissance);";
				echo "\n";
				if ($connect->query($sql))
					echo "L'import de $fic_demande a été effectué.\n";
				else {
					echo "L'import de $fic_demande a echoué (certainement à cause d'une restriction de sécurité du serveur MySQL).\n";
					echo "Veuillez lancer l'import du fichier CSV (au format CRLF) manuellement depuis un shell avec la requête suivante :\n";
					echo "\n";
					echo "mysql --enable-local-infile -u $user_mysql -p$pass_mysql -B -N -D $bdd_mysql -e \"LOAD DATA LOCAL INFILE '$fic_demande' INTO TABLE demande FIELDS TERMINATED BY ';' ENCLOSED BY '\\\"'  LINES TERMINATED BY '\\r\\n' IGNORE 1 LINES (num_ind, sexe, nom_usage, nom_usage_sans, nom_civil, prenom, prenom_sans, annee_naissance, mois_naissance, jour_naissance);\"\n";
					echo "\n";
					echo "\n";
				}
		}
			
			// Inssertion des données tests du fichier "individus.csv"
			if ($entry == $fic_individus) {
			  $connect->query ("truncate table individus");
			  $sql = "LOAD DATA LOCAL INFILE '$fic_individus'
						INTO TABLE individus FIELDS TERMINATED BY ';' ENCLOSED BY '\"'  LINES TERMINATED BY '\r\n'  IGNORE 1 LINES
						(num_ind, sexe, nom_usage, nom_usage_sans, nom_civil, prenom, prenomd, prenomt, annee_naissance, mois_naissance, jour_naissance, adresse, mdr, telephone, mail_mdr, libelle, code);";
				// STARTING BY '' ESCAPED BY '\\'
				echo "\n";
				if ($connect->query($sql))
					echo "L'import de $fic_individus a été effectué.\n";
				else {
					echo "L'import de $fic_individus a echoué (certainement à cause d'une restriction de sécurité du serveur MySQL).\n";
					echo "Veuillez lancer l'import du fichier CSV (au format CRLF) manuellement depuis un shell avec la requête suivante :\n";
					echo "\n";
					echo "mysql --enable-local-infile -u $user_mysql -p$pass_mysql -B -N -D $bdd_mysql -e \"LOAD DATA LOCAL INFILE '$fic_individus' INTO TABLE individus FIELDS TERMINATED BY ';' ENCLOSED BY '\\\"'  LINES TERMINATED BY '\\r\\n'  IGNORE 1 LINES (num_ind, sexe, nom_usage, nom_usage_sans, nom_civil, prenom, prenomd, prenomt, annee_naissance, mois_naissance, jour_naissance, adresse, mdr, telephone, mail_mdr, libelle, code);\"\n";
					echo "\n";
					echo "\n";
				}
		}
	    }
	}
	closedir ( $handle );
}

echo "Fin de l'import de données de tests\n";
?>
